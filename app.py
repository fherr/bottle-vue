import os
import bottle

from bottle import template, PasteServer, run, static_file, response
from random import *
from json import dumps
# Define dirs
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_DIR = os.path.join(BASE_DIR, 'dist/static')

# App Config
bottle.TEMPLATE_PATH.insert(0, os.path.join(BASE_DIR, 'dist'))
bottle.debug(True)  # Don't forget switch this to `False` on production!

# Starting App
app = bottle.default_app()

@app.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


@app.get('/api/random')
def get_random():
    rv = {'randomNumber': randint(1, 100)}
    response.content_type = 'application/json'
    return dumps(rv)

# Index page route
@app.route('/')
@app.route('/<:re>')
def show_index():
    """Show Index page"""
    return template('index')


# Static files route
@app.get('/static/<filename:path>')
def get_static_files(filename):
    """Get Static files"""
    return static_file(filename, root=STATIC_DIR)


# Run server
run(app, server=PasteServer, host='localhost', port=5000, reloader=True)